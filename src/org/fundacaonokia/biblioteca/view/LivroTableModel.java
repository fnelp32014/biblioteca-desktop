/*                                                                                                                                                                                                               
 * Copyright (c) 2014. Andrew C. Pacifico. All rights reserved.                  
 *                                                                               
 * Licensed under the Apache License, Version 2.0 (the "License");               
 * you may not use this file except in compliance with the License.              
 * You may obtain a copy of the License at                                       
 *                                                                               
 *    http://www.apache.org/licenses/LICENSE-2.0                                 
 *                                                                               
 * Unless required by applicable law or agreed to in writing, software           
 * distributed under the License is distributed on an "AS IS" BASIS,             
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.      
 * See the License for the specific language governing permissions and           
 * limitations under the License.                                                
 */
package org.fundacaonokia.biblioteca.view;

import java.util.List;

import javax.swing.table.AbstractTableModel;

import org.fundacaonokia.biblioteca.dao.LivroDAO;
import org.fundacaonokia.biblioteca.model.Livro;

/**
 * Controla os dados associados à tabela utilizada para listagem de livros
 *
 * @author Andrew C. Pacifico <andrewcpacifico@gmail.com>
 */
public class LivroTableModel extends AbstractTableModel {

    private LivroDAO dao;
    private List<Livro> lstLivros;
    
    private static final int NUM_COLUNAS = 3;
    private static final String TITULOS_COLUNAS[] = {"Isbn", "Título", "Autor"};
    
    /**
     * Construtor do model, carrega os dados do banco para a lista de livros 
     */
    public LivroTableModel() {
        this.dao = new LivroDAO();
        this.lstLivros = dao.getAll();
    }
    
    @Override
    public String getColumnName(int column) {
        return TITULOS_COLUNAS[column];
    }
    
    @Override
    public int getRowCount() {
        return this.lstLivros.size();
    }

    @Override
    public int getColumnCount() {
        return NUM_COLUNAS;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Livro l = lstLivros.get(rowIndex);
        
        switch (columnIndex) {
        case 0:
            return l.getIsbn();
            
        case 1:
            return l.getTitulo();
            
        case 2:
            return l.getAutor();
            
        default:
            return null;
        }
    }

}
















