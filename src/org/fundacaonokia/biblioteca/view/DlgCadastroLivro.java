/*                                                                                                                                                                                                               
 * Copyright (c) 2014. Andrew C. Pacifico. All rights reserved.                  
 *                                                                               
 * Licensed under the Apache License, Version 2.0 (the "License");               
 * you may not use this file except in compliance with the License.              
 * You may obtain a copy of the License at                                       
 *                                                                               
 *    http://www.apache.org/licenses/LICENSE-2.0                                 
 *                                                                               
 * Unless required by applicable law or agreed to in writing, software           
 * distributed under the License is distributed on an "AS IS" BASIS,             
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.      
 * See the License for the specific language governing permissions and           
 * limitations under the License.                                                
 */
package org.fundacaonokia.biblioteca.view;

import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerModel;
import javax.swing.SpinnerNumberModel;
import javax.swing.border.BevelBorder;

import net.miginfocom.swing.MigLayout;

import org.fundacaonokia.biblioteca.model.Genero;
import org.fundacaonokia.biblioteca.model.Livro;

/**
 * Diálogo de cadastro de livro
 *
 * @author Andrew C. Pacifico <andrewcpacifico@gmail.com>
 */
public class DlgCadastroLivro extends JDialog {

    /** Instância do livro a ser retornada ao chamar o método novoLivro */
    private Livro lvrRetorno = null;
    
    protected JPanel pnlConteudo;
    protected JPanel pnlBotoes;
    protected JLabel lblIsbn;
    protected JTextField txtIsbn;
    protected JLabel lblAutor;
    protected JTextField txtAutor;
    protected JLabel lblEditora;
    protected JTextField txtEditora;
    protected JLabel lblTitulo;
    protected JTextField txtTitulo;
    protected JLabel lblAno;
    protected JComboBox<Integer> cmbAno;
    protected JLabel lblEdicao;
    protected JSpinner spnEdicao;
    protected JLabel lblNumPaginas;
    protected JSpinner spnNumPaginas;
    protected JLabel lblGeneros;
    protected JList<Genero> lstGeneros;
    protected JButton btnAddGenero;
    protected JButton btnRmGenero;
    protected DefaultListModel<Genero> lstGenerosModel;
    protected DefaultListModel<Genero> lstGenDispModel;
    protected JList<Genero> lstGenDisponiveis;
    protected JPanel contentPane;
    protected JButton btnOk;
    protected JButton btnCancelar;
    
    public DlgCadastroLivro() {
        initComponents();
    }
    
    /**
     * Cofigura os componentes visuais da tela
     */
    private void initComponents() {
        /*----------------------------------------------------------------------
         * Painel principal
         * -------------------------------------------------------------------*/
        this.contentPane = new JPanel();
        this.contentPane.setLayout(new MigLayout("", "0[grow]0", "0[]30[]0"));
        this.setContentPane(this.contentPane);
        
        this.pnlConteudo = new JPanel();
        this.pnlConteudo.setLayout(new MigLayout("", "50[150][150][]50", 
                "50[][]20[][]20[][]20[][]20[][]"));
        
        this.lblIsbn = new JLabel("Isbn");
        this.pnlConteudo.add(this.lblIsbn, "cell 0 0");
        
        this.txtIsbn = new JTextField();
        this.pnlConteudo.add(this.txtIsbn, "cell 0 1 2 1, w 300");
        
        this.lblAutor = new JLabel("Autor");
        this.pnlConteudo.add(this.lblAutor, "cell 0 2");
        
        this.txtAutor = new JTextField();
        this.pnlConteudo.add(this.txtAutor, "cell 0 3 2 1, w 300, id txtAutor");
        
        this.lblEditora = new JLabel("Editora");
        this.pnlConteudo.add(this.lblEditora, "cell 2 2");
        
        this.txtEditora = new JTextField();
        this.pnlConteudo.add(this.txtEditora, "cell 2 3, w 300, id txtEditora");
        
        this.lblTitulo = new JLabel("Titulo");
        this.pnlConteudo.add(this.lblTitulo, "cell 0 4");
        
        this.txtTitulo = new JTextField();
        this.pnlConteudo.add(this.txtTitulo, "cell 0 5 3 1, x txtAutor.x, x2 txtEditora.x2");
        
        this.lblAno = new JLabel("Ano");
        this.pnlConteudo.add(this.lblAno, "cell 0 6");
        
        this.cmbAno = new JComboBox<Integer>();
        for (int i = 1900; i <= 2014; i++) {
            this.cmbAno.addItem(i);
        }
        this.cmbAno.setSelectedItem(2014);
        this.pnlConteudo.add(this.cmbAno, "cell 0 7, growx");
        
        this.lblEdicao = new JLabel("Edição");
        this.pnlConteudo.add(this.lblEdicao, "cell 1 6");
        
        SpinnerModel spnEdicaoModel = new SpinnerNumberModel(1, 1, 500, 1);
        this.spnEdicao = new JSpinner(spnEdicaoModel);
        this.pnlConteudo.add(this.spnEdicao, "cell 1 7, growx");
        
        this.lblNumPaginas = new JLabel("Nº Páginas");
        this.pnlConteudo.add(this.lblNumPaginas, "cell 2 6");
        
        SpinnerModel spnNumPaginasModel = new SpinnerNumberModel(1, 1, 10000, 1);
        this.spnNumPaginas = new JSpinner(spnNumPaginasModel);
        this.pnlConteudo.add(this.spnNumPaginas, "cell 2 7, w 150::150");
        
        this.lblGeneros = new JLabel("Gêneros");
        this.pnlConteudo.add(this.lblGeneros, "cell 0 8");
        
        this.lstGenerosModel = new DefaultListModel<Genero>();
        this.lstGeneros = new JList<Genero>(this.lstGenerosModel);
        this.pnlConteudo.add(this.lstGeneros, "cell 0 9 2 1, growx, w 300::300, h 200::200");
        
        this.lstGenDispModel = new DefaultListModel<Genero>();
        this.lstGenDispModel.addElement(new Genero(0, "Ficção"));
        this.lstGenDispModel.addElement(new Genero(1, "Juvenil"));
        this.lstGenDispModel.addElement(new Genero(2, "Auto-Ajuda"));
        this.lstGenDisponiveis = new JList<Genero>(this.lstGenDispModel);
        this.pnlConteudo.add(this.lstGenDisponiveis, "cell 2 9 2 1, growx, w 300::300, h 200::200");
        
        this.btnAddGenero = new JButton("<< Adicionar");
        this.pnlConteudo.add(this.btnAddGenero, "cell 2 10 2 1, al center, w 200::200");
        
        this.btnRmGenero = new JButton("Remover >>");
        this.pnlConteudo.add(this.btnRmGenero, "cell 0 10 2 1, al center, w 200::200");
        
        this.contentPane.add(this.pnlConteudo, "cell 0 0, growx");
        
        /*----------------------------------------------------------------------
         * Configurações do painel de botões inferior
         * -------------------------------------------------------------------*/
        this.pnlBotoes = new JPanel();
        this.pnlBotoes.setBorder(new BevelBorder(BevelBorder.RAISED));
        this.pnlBotoes.setLayout(new MigLayout("ax right, ay center", "[]20[]50", "[]"));
        this.contentPane.add(this.pnlBotoes, "cell 0 1, growx, h 80::80");
        
        this.btnOk = new JButton("Ok");
        this.btnOk.setIcon(new ImageIcon("img/Check_16x16.png"));
        this.pnlBotoes.add(this.btnOk, "cell 1 0, w 100::100");
        
        this.btnCancelar = new JButton("Cancelar");
        this.btnCancelar.setIcon(new ImageIcon("img/Cancel_16x16.png"));
        this.pnlBotoes.add(this.btnCancelar, "cell 0 0, w 100::100");
        
        /*----------------------------------------------------------------------
         * Configurações da janela
         * -------------------------------------------------------------------*/
        this.setTitle("Cadastro de Livro");
        this.pack();
    }

    /**
     * Método responsável por exibir o Frame e retornar uma instância de livro
     * com os dados informados pelo usuário
     * 
     * @return Instância do livro com os dados informados pelo usuário, ou null
     *          caso o usuário pressione o botão cancelar, ou feche a janela.
     */
    public Livro novoLivro() {
        this.setModal(true);
        this.setVisible(true);
        
        return this.lvrRetorno;
    }
    
}
