/*                                                                                                                                                                                                               
 * Copyright (c) 2014. Andrew C. Pacifico. All rights reserved.                  
 *                                                                               
 * Licensed under the Apache License, Version 2.0 (the "License");               
 * you may not use this file except in compliance with the License.              
 * You may obtain a copy of the License at                                       
 *                                                                               
 *    http://www.apache.org/licenses/LICENSE-2.0                                 
 *                                                                               
 * Unless required by applicable law or agreed to in writing, software           
 * distributed under the License is distributed on an "AS IS" BASIS,             
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.      
 * See the License for the specific language governing permissions and           
 * limitations under the License.                                                
 */

package org.fundacaonokia.biblioteca.view;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.border.BevelBorder;

import org.fundacaonokia.biblioteca.model.Livro;

import net.miginfocom.swing.MigLayout;

/**
 * Implementação da tela principal do sistema.
 * 
 * @author Andrew C. Pacifico <andrewcpacifico@gmail.com>
 */
public class FrmPrincipal extends JFrame {

    protected JPanel contentPane;
    protected JPanel pnlTopo;
    protected JScrollPane pnlConteudo;
    protected JPanel pnlBotoes;
    protected JButton btnInserir;
    protected JButton btnEditar;
    protected JMenuBar menuBar;
    protected JMenu mnuArquivo;
    protected JMenuItem mnitFechar;
    protected JButton btnExcluir;
    protected JButton btnPesquisar;
    protected JTextField txtPesquisa;
    protected JComboBox<String> cmbCriterio;
    protected JLabel lblCriterio;
    protected JLabel lblTexPesquisa;
    protected JTable tblLivros;
    protected LivroTableModel tblLivrosModel;

    /**
     * Construtor da classe
     */
    public FrmPrincipal() {
        initComponents();
    }

    /**
     * Método responsável por inicializar os componentes visuais da tela.
     */
    public void initComponents() {
        this.contentPane = new JPanel();
        this.contentPane.setLayout(new MigLayout("", "0[grow]0", "0[]0[grow]0[]0"));

        /*----------------------------------------------------------------------
         * Barra de Menus
         * -------------------------------------------------------------------*/
        this.menuBar = new JMenuBar();
        this.setJMenuBar(this.menuBar);

        /* Menu Arquivo */
        this.mnuArquivo = new JMenu("Arquivo");
        this.mnuArquivo.setMnemonic('A');
        this.menuBar.add(this.mnuArquivo);

        /* Menu Arquivo > Fechar */
        this.mnitFechar = new JMenuItem("Fechar");
        this.mnitFechar.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Q,
                InputEvent.CTRL_MASK));
        this.mnitFechar.setMnemonic('F');
        this.mnitFechar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
        this.mnuArquivo.add(this.mnitFechar);

        /*----------------------------------------------------------------------
         * Painel superior
         * -------------------------------------------------------------------*/
        this.pnlTopo = new JPanel();
        this.pnlTopo.setLayout(new MigLayout("", "20[]20[grow]20[]20", "0[120::120]0"));
        this.contentPane.add(this.pnlTopo, "cell 0 0, h 120::120, growx");
        
        /* Label texto pesquisa */
        this.lblTexPesquisa = new JLabel("Texto da pesquisa");
        this.pnlTopo.add(this.lblTexPesquisa, "cell 1 0, flowy");
        
        /* TextField da pesquisa */
        this.txtPesquisa = new JTextField();
        this.pnlTopo.add(this.txtPesquisa, "cell 1 0, w 300, growx, id txtPesquisa, aligny center");
        
        /* Label critério */
        this.lblCriterio = new JLabel("Critério de pesquisa");
        this.pnlTopo.add(this.lblCriterio, "cell 0 0, flowy");
        
        /* Combobox de critério de pesquisa */
        this.cmbCriterio = new JComboBox<String>(new String[] {
                "Titulo", "Autor", "Editora" });
        this.pnlTopo.add(this.cmbCriterio, "cell 0 0");
        
        /* Botão de pesquisa */
        this.btnPesquisar = new JButton("Pesquisar");
        this.btnPesquisar.setIcon(new ImageIcon("img/Search_16x16.png"));
        this.pnlTopo.add(this.btnPesquisar, "cell 2 0, y txtPesquisa.y");

        
        this.pnlConteudo = new JScrollPane();
        this.pnlConteudo.setBorder(BorderFactory.createBevelBorder(BevelBorder.LOWERED));
        this.contentPane.add(this.pnlConteudo, "cell 0 1, grow");

        /*----------------------------------------------------------------------
         * Painel inferior com os botões.
         * -------------------------------------------------------------------*/
        this.pnlBotoes = new JPanel();
        this.pnlBotoes.setLayout(new MigLayout("", "[grow][]15[]15[]20", "[grow]"));
        this.contentPane.add(this.pnlBotoes, "cell 0 2, h 80::80, growx");

        /* Botão de inserção */
        this.btnInserir = new JButton("Inserir");
        this.btnInserir.setPreferredSize(new Dimension(120, 50));
        this.btnInserir.setIcon(new ImageIcon("img/Add_32x32.png"));
        this.btnInserir.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                btnInserirActionPerformed(e);
            }
        });
        this.pnlBotoes.add(this.btnInserir, "cell 3 0");

        /* Botão de edição */
        this.btnEditar = new JButton("Editar");
        this.btnEditar.setPreferredSize(new Dimension(120, 50));
        this.btnEditar.setIcon(new ImageIcon("img/Edit_32x32.png"));
        this.pnlBotoes.add(this.btnEditar, "cell 2 0");
        
        /* Botão de exclusão */
        this.btnExcluir = new JButton("Excluir");
        this.btnExcluir.setPreferredSize(new Dimension(120, 50));
        this.btnExcluir.setIcon(new ImageIcon("img/Delete_32x32.png"));
        this.pnlBotoes.add(this.btnExcluir, "cell 1 0");

        setContentPane(contentPane);
        setSize(950, 550);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        /*----------------------------------------------------------------------
         * Painel de conteúdo
         * -------------------------------------------------------------------*/
        this.tblLivros = new JTable();
        this.pnlConteudo.setViewportView(this.tblLivros);
        
        this.tblLivrosModel = new LivroTableModel();
        this.tblLivros.setModel(this.tblLivrosModel);
    }

    /**
     * Implementação da ação do botão de inserção de livro
     * 
     * @param e Instância do evento
     */
    protected void btnInserirActionPerformed(ActionEvent e) {
        DlgCadastroLivro viewCadastro = new DlgCadastroLivro();
        Livro novo = viewCadastro.novoLivro();
    }

}










