/*                                                                                                                                                                                                               
 * Copyright (c) 2014. Andrew C. Pacifico. All rights reserved.                  
 *                                                                               
 * Licensed under the Apache License, Version 2.0 (the "License");               
 * you may not use this file except in compliance with the License.              
 * You may obtain a copy of the License at                                       
 *                                                                               
 *    http://www.apache.org/licenses/LICENSE-2.0                                 
 *                                                                               
 * Unless required by applicable law or agreed to in writing, software           
 * distributed under the License is distributed on an "AS IS" BASIS,             
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.      
 * See the License for the specific language governing permissions and           
 * limitations under the License.                                                
 */
package org.fundacaonokia.biblioteca.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.fundacaonokia.biblioteca.lib.SimpleConnectionFactory;
import org.fundacaonokia.biblioteca.model.Livro;

/**
 * Classe de manipulação de dados dos livros
 *
 * @author Andrew C. Pacifico <andrewcpacifico@gmail.com>
 */
public class LivroDAO {

    /** Conexão com o banco de dados */
    private Connection conn;

    /**
     * Construtor da classe
     */
    public LivroDAO() {
        this.conn = SimpleConnectionFactory.getConnection();
    }

    /**
     * Recupera todos os livros do banco de dados
     * 
     * @return Lista contendo todos os registros de livros cadastrados.
     */
    public List<Livro> getAll() {
        List<Livro> lstRetorno = new ArrayList<Livro>();

        String sql = "SELECT * FROM Livro";
        try {
            PreparedStatement pstm = this.conn.prepareStatement(sql);
            ResultSet rs = pstm.executeQuery();

            while (rs.next()) {
                // String str = rs.getString("titulo");
                Livro l = new Livro(rs.getString("isbn"),
                        rs.getString("autor"), rs.getString("titulo"),
                        rs.getString("editora"), rs.getInt("ano"),
                        rs.getInt("edicao"), rs.getInt("numPaginas"), null);
                lstRetorno.add(l);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return lstRetorno;
    }

    /**
     * Insere o registro de um novo livro no banco de dados.
     * 
     * @param l
     *            Instância do livro que será cadastrado.
     */
    public void novo(Livro l) {
        String sql = "INSERT INTO Livro (isbn, titulo, autor, editora, ano, "
                + "edicao, numPaginas) VALUES (?, ?, ?, ?, ?, ?, ?)";
        
        try {
            PreparedStatement pstm = this.conn.prepareStatement(sql);
            
            pstm.setString(1, l.getIsbn());
            pstm.setString(2, l.getTitulo());
            pstm.setString(3, l.getAutor());
            pstm.setString(4, l.getEditora());
            pstm.setInt(5, l.getAno());
            pstm.setInt(6, l.getEdicao());
            pstm.setInt(7, l.getNumPaginas());
            
            pstm.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}




















