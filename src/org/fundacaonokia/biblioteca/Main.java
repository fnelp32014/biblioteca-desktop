/*                                                                                                                                                                                                               
 * Copyright (c) 2014. Andrew C. Pacifico. All rights reserved.                  
 *                                                                               
 * Licensed under the Apache License, Version 2.0 (the "License");               
 * you may not use this file except in compliance with the License.              
 * You may obtain a copy of the License at                                       
 *                                                                               
 *    http://www.apache.org/licenses/LICENSE-2.0                                 
 *                                                                               
 * Unless required by applicable law or agreed to in writing, software           
 * distributed under the License is distributed on an "AS IS" BASIS,             
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.      
 * See the License for the specific language governing permissions and           
 * limitations under the License.                                                
 */

package org.fundacaonokia.biblioteca;

import java.util.List;

import javax.swing.UIManager;
import javax.swing.UIManager.LookAndFeelInfo;
import javax.swing.UnsupportedLookAndFeelException;

import org.fundacaonokia.biblioteca.dao.LivroDAO;
import org.fundacaonokia.biblioteca.model.Livro;
import org.fundacaonokia.biblioteca.view.DlgCadastroLivro;
import org.fundacaonokia.biblioteca.view.FrmPrincipal;

/**
 * Classe principal do sistema. Realiza as configurações gerais, e inicializa
 * a primeira tela.
 *  
 * @author Andrew C. Pacifico <andrewcpacifico@gmail.com>
 */
public class Main {

    public static void main(String[] args) {
        /* Define o look and feel do sistema */
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (ClassNotFoundException | InstantiationException
                | IllegalAccessException | UnsupportedLookAndFeelException e) {
            e.printStackTrace();
        }
        
        FrmPrincipal viewPrincipal = new FrmPrincipal();
        viewPrincipal.setVisible(true);
//        
//        LivroDAO dao = new LivroDAO();
//        List<Livro> lstLivros = dao.getAll();
//        
//        for (Livro l : lstLivros) {
//            System.out.println(l);
//        }
    }
    
}










