/*                                                                                                                                                                                                               
 * Copyright (c) 2014. Andrew C. Pacifico. All rights reserved.                  
 *                                                                               
 * Licensed under the Apache License, Version 2.0 (the "License");               
 * you may not use this file except in compliance with the License.              
 * You may obtain a copy of the License at                                       
 *                                                                               
 *    http://www.apache.org/licenses/LICENSE-2.0                                 
 *                                                                               
 * Unless required by applicable law or agreed to in writing, software           
 * distributed under the License is distributed on an "AS IS" BASIS,             
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.      
 * See the License for the specific language governing permissions and           
 * limitations under the License.                                                
 */
package org.fundacaonokia.biblioteca.model;

import java.util.List;

/**
 * Classe modelo de um Livro.
 * 
 * @author Andrew C. Pacifico <andrewcpacifico@gmail.com>
 */
public class Livro {
    private String isbn;
    private String autor;
    private String titulo;
    private String editora;
    private int ano;
    private int edicao;
    private int numPaginas;
    private List<String> generos;

    public Livro(String isbn, String autor, String titulo, String editora,
            int ano, int edicao, int numPaginas, List<String> generos) {
        super();
        this.isbn = isbn;
        this.autor = autor;
        this.titulo = titulo;
        this.editora = editora;
        this.ano = ano;
        this.edicao = edicao;
        this.numPaginas = numPaginas;
        this.generos = generos;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public String getAutor() {
        return autor;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getEditora() {
        return editora;
    }

    public void setEditora(String editora) {
        this.editora = editora;
    }

    public int getAno() {
        return ano;
    }

    public void setAno(int ano) {
        this.ano = ano;
    }

    public int getEdicao() {
        return edicao;
    }

    public void setEdicao(int edicao) {
        this.edicao = edicao;
    }

    public int getNumPaginas() {
        return numPaginas;
    }

    public void setNumPaginas(int numPaginas) {
        this.numPaginas = numPaginas;
    }

    public List<String> getGeneros() {
        return generos;
    }

    public void setGeneros(List<String> generos) {
        this.generos = generos;
    }

    @Override
    public String toString() {
        return "Livro [isbn=" + isbn + ", autor=" + autor + ", titulo="
                + titulo + ", editora=" + editora + ", ano=" + ano
                + ", edicao=" + edicao + ", numPaginas=" + numPaginas
                + ", generos=" + generos + "]";
    }
    
}
