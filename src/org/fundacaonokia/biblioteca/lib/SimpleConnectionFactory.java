/*                                                                                                                                                                                                               
 * Copyright (c) 2014. Andrew C. Pacifico. All rights reserved.                  
 *                                                                               
 * Licensed under the Apache License, Version 2.0 (the "License");               
 * you may not use this file except in compliance with the License.              
 * You may obtain a copy of the License at                                       
 *                                                                               
 *    http://www.apache.org/licenses/LICENSE-2.0                                 
 *                                                                               
 * Unless required by applicable law or agreed to in writing, software           
 * distributed under the License is distributed on an "AS IS" BASIS,             
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.      
 * See the License for the specific language governing permissions and           
 * limitations under the License.                                                
 */
package org.fundacaonokia.biblioteca.lib;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * SimpleConnectionFactory
 * 
 * Classe que gerencia as conexões com o banco de dados.
 *
 * @author Andrew C. Pacifico <andrewcpacifico@gmail.com>
 */
public class SimpleConnectionFactory {

    private static String dbUser = "biblioteca";
    private static String dbPass = "biblioteca";
    private static String connString = "jdbc:mysql://localhost/biblioteca";
    
    /**
     * Método responsável por estabelecer uma conexão com o banco de dados
     * 
     * @return Objeto contendo a conexão com o banco, ou null caso ocorra algum erro.
     */
    public static Connection getConnection() {
        Connection conRetorno = null;
        
        try {
            conRetorno = DriverManager.getConnection(connString, dbUser, dbPass);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        
        return conRetorno;
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
}
